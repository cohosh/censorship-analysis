This is a collection of tools for analyzing Tor blocking events.

- `default-bridge-metrics` contains a script to plot the `ips` field of the extrainfo descriptors of our default bridges over time, by country code
